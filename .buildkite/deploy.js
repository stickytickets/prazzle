
if(!process.env.S3_PUBLISH_REGION)  throw `Must supply S3_PUBLISH_REGION env variable`;
if(!process.env.S3_PUBLISH_LOCATION)  throw `Must supply S3_PUBLISH_LOCATION env variable`;

var cmd = `aws s3 cp prazzle.zip s3://${process.env.S3_PUBLISH_LOCATION} --region ${process.env.S3_PUBLISH_REGION}`;

console.log(cmd);
code = require('child_process').execSync(cmd);
