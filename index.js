
exports.setConfig = require('./lib/set-config.js');
exports.publish  = require('./lib/publish.js');
exports.createSpecification = require('./lib/create-specification.js');
exports.deployPackage = require('./lib/deploy-package.js');
