var extend = require('util')._extend;
var assert = require('better-assert');
var async = require('async');
var fs = require('fs');
var path = require('path');
var spawn = require('child_process').spawn;
var zipHelper = require('./utils/zipHelper');
var	log = require('winston');
var source, target, log, isWinOS, logLevel, packageType, metadataFile, installOptions;

exports.run = function(options) {
	assert(options);

	source = options.source;

	metadataFile = source + '.prazzle.json';

	installOptions = { log: log };

	//load package settings
	if (fs.existsSync(metadataFile)) {
		installOptions = extend(installOptions, require(path.resolve(metadataFile)));
		installOptions.project = installOptions.project || installOptions.name;
	}

	target = options.target;
	isWinOS = options.isWinOS;
	packageType = options.packageType;


	assert('string' == typeof source);
	assert('string' == typeof target);

	log.info('Deploying... (source: "%s", target: "%s")', source, target);

	async.waterfall([
		unzipPackage,
		installPackage
	], function(error){
		if (error) throw error;
		log.info('Done');
	});
}

function unzipPackage(callback) {
	var zip = source;

	if (!fs.existsSync(zip)) throw 'Source file does not exist: ' + source;

	zipHelper.unZip(zip, target, function(e) { callback(e) });
}


var installer  = require('./install');

function installPackage(callback){
	var dir = process.cwd();

	process.chdir(target);

	installer.run(installOptions, function(){
		process.chdir(dir);
	});
}
