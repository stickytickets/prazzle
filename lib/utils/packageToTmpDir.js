var path = require('path');
var ncp = require('ncp').ncp;

module.exports = function(target, callback) {
    cb = function() { callback(null, target); };
    return copyCurrentDirTo(target, cb);
}

function copyCurrentDirTo(target, callback) {
    var gitignore = require('parse-gitignore'),
    patterns = gitignore('.npmignore').concat(['env.required']),
    files = require('glob').sync('**/*', { ignore: patterns }),
    currentDir = process.cwd();

    var filter = function(filepath) {
        var file = path.relative(currentDir, filepath.toString()).replace(/\\/g, '/');
        return file === '' ? true : files.indexOf(file) !== -1;
    };

    ncp(currentDir, target, { filter: filter }, callback);
}
