var fs = require('fs');
var path = require('path');

exports.read = function(file) {
	var filePath = path.resolve(file);

	if (fs.existsSync(filePath)) {
		var env = fs.readFileSync(filePath).toString();

		return exports.readString(env);
	}
	else {
		console.warn('Env file not found: ' + file);
	}

	return {};
}

exports.readString = function(text) {
	var keys = {};

	text.split('\n').forEach(function(line) {
		if (!line) return;

		line = line.trim();

		var lineSplit = line.split('=');
		var key = lineSplit[0];
		var value = lineSplit.length > 1 ? lineSplit.slice(1).join('=') : null;

		if (!key) return;

		if (key[0] === '#') return;

		keys[key] = value || '';
	});

	return keys;
}

exports.write = function(file, values) {
	var env = '';

	if (fs.existsSync(file)) {
		env = fs.readFileSync(file).toString();
	}

	for (var k in values) {
		var regex =  new RegExp(k + '.*=');
		var regexReplace = new RegExp(k + '.*=(.*)');
		var value = k + '=' + (values[k] ? values[k] : '');

		if (env.match(regex)) {
			env = env.replace(regexReplace,  value);
		} else {
			env +=  '\n' + value;
		}
	}

	fs.writeFileSync(file, env);
}

exports.writeString = function(values) {
	var content = [];

	for(var k in values) {
		content.push(k + '=' + (values[k] ? values[k] : ''));
	}

	return content.join('\n');
}

exports.writeKeys = function(file, values) {
	var env = '';

	if (fs.existsSync(file)) {
		env = fs.readFileSync(file).toString();
	}

	for(var k in values) {
		var regex =  new RegExp(k + '.*=');
		var regexReplace = new RegExp(k + '.*=(.*)');
		var value = k + '=';

		if (env.match(regex)) {
			env = env.replace(regexReplace,  value);
		} else {
			env +=  '\n' + value;
		}
	}

	fs.writeFileSync(file, env);
}
