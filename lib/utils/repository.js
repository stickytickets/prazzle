var AWS = require('aws-sdk');
var path = require('path');
var fs = require('fs');
var assert = require('assert');
var async = require('async');
var extend = require('util')._extend;
var log = require('winston')
var md5Helper = require('./md5Helper');

var packageKeyBase = 'apps';
var envHelper = require('./envHelper');


module.exports = function(options) {

    var s3Bucket = options.s3Bucket;
    var s3PublicBucket = options.s3BucketPublic;
    var s3Region = options.s3BucketRegion || process.env.PRAZZLE_BUCKET_REGION || 'us-east-1';

    assert('string' == typeof s3Bucket);

    return {

        uploadPackageZip: function(options, callback) {


            assert('object' == typeof options);
            assert('object' == typeof options.metadata);

            options.metadata.name = options.metadata.name || options.metadata.project;


            assert('string' == typeof options.metadata.name);
            assert('string' == typeof options.metadata.version);
            assert('string' == typeof options.metadata.packageType);

            var bucket = options.s3Bucket || s3Bucket;
            var extension = path.extname(options.file);
            var packageJson = options.metadata;

            var s3 = new AWS.S3({
                region: s3Region,
                httpOptions: {
                    timeout: 5 * 60 * 1000
                }
            });


            //Calc md5
            md5Helper.fromFile(options.file, function(md5) {

                var filename = packageJson.version + '-' + md5 + extension;
                packageJson.md5 = md5;
                packageJson.filename = filename;

                async.parallel([
                    function(cb) {

                        var key = [packageKeyBase, packageJson.name, filename].join('/'),
                            params = {
                                Bucket: bucket,
                                Key: key
                            };

                        s3.headObject(params, function(err, r) {
                            //if it doesn't exist already
                            if (!r) {

                                params.Body = fs.createReadStream(options.file);
                                params.Metadata = {
                                    md5: md5
                                };
                                params.ACL = "bucket-owner-full-control";

                                s3.upload(params, function(err, data) {
                                    if (err) {
                                        console.log('Error uploading package file');
                                        console.log(params);
                                        console.error(err);
                                        cb(err);
                                    }
                                    cb(null, "OK");
                                });
                            } else {
                                cb(null, "EXISTS");
                            }
                        })
                    },
                    function(cb) {
                        var key = [packageKeyBase, packageJson.name, packageJson.version + '.json'].join('/'),
                            params = {
                                ACL: "bucket-owner-full-control",
                                Bucket: bucket,
                                Key: key,
                                Body: JSON.stringify(packageJson)
                            };

                        s3.upload(params, function(err, data) {
                            if (err) {
                                console.log('Error uploading package json');
                                console.error(err);
                                cb(err);
                            }

                            cb(null, "OK");
                        });
                    }
                ], callback);

            });

        },


        downloadPackageZip: function(options, cb) {

            assert('object' == typeof options);
            assert('string' == typeof options.name);
            assert('string' == typeof options.version);

            var packageName = options.name;
            var packageVersion = options.version;
            var toPath = options.zipFilename;

            var mkdir = require('mkdirp');
            mkdir.sync(path.dirname(toPath));

            checkExistingMd5(toPath, options.md5, function(exists) {

                if (exists) return cb(null, options);

                //Download From S3.
                var s3 = new AWS.S3( {region: s3Region}),
                    key = [packageKeyBase, packageName, options.filename].join('/'),
                    params = {
                        Bucket: s3Bucket,
                        Key: key
                    };

                log.debug('downloading s3: ', params, 'to', toPath);

                var file = require('fs').createWriteStream(toPath);
                var readable = s3.getObject(params).createReadStream();

                readable.pipe(file);
                readable.on('error', function(e) {
                    log.error("Error downloading:", params.Key);
                    console.log(e);
                    throw e;
                });

                file.on('close', function() {
                    log.debug('download complete:', params);
                    cb(null, options);
                });

            })
        },

        uploadBrowser: function(options, callback) {
            options.s3Bucket = s3PublicBucket;
            this.uploadPackageZip(options, callback);
        },


        //gets the .env file fro the current environment.
        // downloads /environments/[environment-name/projects/[project-name].env
        // Parses file and returns a object map, e.g { host: "www.stickytickets.com.au", env: "staging" }
        getPackageMetaData: function(options, cb) {
            assert('object' == typeof options);
            assert('string' == typeof options.project);
            assert('string' == typeof options.version);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Key: [packageKeyBase, options.project, options.version + '.json'].join('/')
                };

            log.debug('getting package required keys: %j', params)

            s3.getObject(params, function(err, data) {

                if (err && err.code === 'NoSuchKey') return cb("The package doesn't exist", null);
                if (err && err.code === 'NotFound') return cb("The package doesn't exist", null);

                if (err) {
                    return cb(err);
                }

                cb(null, JSON.parse(data.Body.toString()));
            });
        },

        //gets the .env file fro the current environment.
        // downloads /environments/[environment-name/projects/[project-name].env
        // Parses file and returns a object map, e.g { host: "www.stickytickets.com.au", env: "staging" }
        getEnv: function(options, cb) {

            assert('object' == typeof options);
            assert('string' == typeof options.project);
            assert('string' == typeof options.environment);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'env', options.project + '.env'].join('/')
                };

            s3.getObject(params, function(err, data) {


                if (err && err.code === 'NoSuchKey') return cb(null, {});

                if (err) return cb(err);

                cb(null, envHelper.readString(data.Body.toString()));
            });
        },


        // gets the version number for an environment
        // environments/[environment-name/projects/[project-name]/version.txt
        // Reads first line of text from file, trims and returns (e.g  returns "1.0.12-develop" )
        getVersion: function(environment, projectName) {

        },


        removeConfig: function(options, callback) {

            options.name = options.name || options.project;

            assert('object' == typeof options);
            assert('string' == typeof options.environment);
            assert('string' == typeof options.name);



            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'projects', options.name + '.json'].join('/')
                };





            s3.deleteObject(params, function(err, data) {
                if (err) cb(err);
                callback(null, "OK");
            });


        },

        // sets the version number for an environment
        // saves to environments/[environment-name/projects/[project-name]/version.txt
        // Writes given version to a text from file
        setConfig: function(options, callback) {

            assert('object' == typeof options);
            assert('string' == typeof options.environment);



            var metadata = options.config;
            metadata.name = metadata.name || options.project;


            assert('object' == typeof metadata);
            assert('string' == typeof metadata.name);
            assert('string' == typeof metadata.packageType);

            assert('string' == typeof metadata.version);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    ACL: "bucket-owner-full-control",
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'projects', metadata.name + '.json'].join('/'),
                    Body: JSON.stringify(metadata),
                };


            options.project = options.name;
            this.getEnv({
                project: metadata.name,
                environment: options.environment
            }, function(defaults) {


                async.parallel([
                    function(cb) {

                        //Update the default environment settings for this project.
                        defaults = extend(defaults || {}, metadata.env);



                        if (isEmpty(defaults)) return cb(null, "OK");

                        var envParams = {
                            ACL: "bucket-owner-full-control",
                            Bucket: s3Bucket,
                            Key: ['environments', options.environment, 'env', metadata.name + '.env'].join('/'),
                            Body: envHelper.writeString(defaults)
                        };

                        s3.upload(envParams, function(err, data) {
                            if (err) cb(err);
                            cb(null, "OK");
                        });
                    },
                    function(cb) {
                        s3.upload(params, function(err, data) {
                            if (err) cb(err);
                            cb(null, "OK");
                        });
                    }
                ], callback);





            });
        },
        getConfig: function(options, cb) {
            assert('object' == typeof options);
            assert('string' == typeof options.project);
            assert('string' == typeof options.environment);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'projects', options.project + '.json'].join('/'),
                };


            s3.getObject(params, function(err, data) {

                if (err && err.code === 'NoSuchKey') return cb(null, null);

                if (err) {
                    console.log('Getting: %j , region', params, process.env.AWS_REGION);
                    console.error(err);
                    return cb(err)
                };

                cb(null, JSON.parse(data.Body.toString()));
            });
        },


        // gets the tags
        // reads /tags/[tagName] - file is a list of tag names seperated by a new line
        getTag: function(tagName) {

        },

        // sets the version number for an environment
        // saves to environments/[environment-name/projects/[project-name]/version.txt
        // Writes given version to a text from file
        setTag: function(tagName, valuesArray) {

        },
        getPackageList: function(options, cb) {
            assert('object' == typeof options);
            assert('string' == typeof options.environment);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Prefix: ['environments', options.environment, 'projects'].join('/')
                };

            s3.listObjects(params, function(err, data) {
                if (err) cb(err);
                var results = [];
                for (var i = 0; i < data.Contents.length; i++) {
                    var key = data.Contents[i].Key,
                        name = key.split('/').pop();
                    if (name) results.push(path.basename(name, ".json"));
                }

                cb(null, results);
            });
        },
        setSpec: function(options, callback) {

            assert('object' == typeof options);
            assert('object' == typeof options.spec);
            assert('string' == typeof options.environment);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    ACL: "bucket-owner-full-control",
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'specification', 'current.json'].join('/'),
                    Body: JSON.stringify(options.spec)
                };

            var historyEntry = {
                ACL: "bucket-owner-full-control",
                Bucket: s3Bucket,
                Key: ['environments', options.environment, 'specification', 'z-' + new Date().toISOString() + '.json'].join('/'),
                Body: JSON.stringify(options.spec)
            };

            async.parallel([
                function(cb) {
                    s3.upload(params, function(err, data) {
                        if (err) cb(err);
                        cb(null, "OK");
                    });
                },
                function(cb) {
                    s3.upload(historyEntry, function(err, data) {
                        if (err) cb(err);
                        cb(null, "OK");
                    });
                }
            ], callback);


        },

        getSpec: function(options, cb) {
            assert('object' == typeof options);
            assert('string' == typeof options.environment);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'specification', 'current.json'].join('/')
                };

            s3.getObject(params, function(err, data) {
                if (err) return cb(err);
                cb(null, JSON.parse(data.Body.toString()));
            });

        },

        getSecretList: function(options, cb) {
            assert('object' == typeof options);
            assert('string' == typeof options.environment);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Prefix: ['environments', options.environment, 'secrets'].join('/')
                };

            s3.listObjects(params, function(err, data) {
                if (err) cb(err);
                var results = [];

                for (var i = 0; i < data.Contents.length; i++) {
                    var key = data.Contents[i].Key,
                        name = key.split('/').pop();
                    results.push(path.basename(name));
                }

                cb(null, results);
            });
        },

        getSecret: function(options, cb) {
            assert('object' == typeof options);
            assert('string' == typeof options.secretName);

            var s3 = new AWS.S3({region: s3Region}),
                params = {
                    Bucket: s3Bucket,
                    Key: ['environments', options.environment, 'secrets', options.secretName].join('/')
                };

            s3.getObject(params, function(err, data) {
                if (err) return cb(err);
                cb(null, data.Body.toString());
            });
        }
    };
}


function checkExistingMd5(toPath, packageMd5, cb) {
    //MD5 Any Existing zip.
    if (fs.existsSync(toPath)) {
        return md5Helper.fromFile(toPath, function(md5) {
            cb(md5 == packageMd5);
        })
    }
    cb(false);
}

function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true && JSON.stringify(obj) === JSON.stringify({});
}
