var path = require('path');
var  log = require('winston');
var tmp = require('tmp');

module.exports.load = function(packageType){


	var plugin;
	var pluginPackageName = 'prazzle-package-' + packageType;

	var pluginDir = path.join(process.cwd(), 'node_modules', pluginPackageName);

	var localPluginFound;

	var localError;
	//Try Local
	try
	{
		plugin = require(pluginDir);
		localPluginFound = true;
		return plugin;
	}
	catch (e) 	{
		localError = e;

	}
	//Try Global

	return require(pluginPackageName);
	
}

module.exports.configure = function(options){

	tmp.setGracefulCleanup();
	tempDir = tmp.dirSync({ prefix: 'prazzle-', unsafeCleanup: true }).name;

	options.tmpDir = tempDir;
	options.fn = {
		gitVersion: require('./gitVersion'),
		envHelper: require('./envHelper'),
		zipHelper: require('./zipHelper'),
		cleanDir: require('./cleanDir'),
		packageToTmpDir: require('./packageToTmpDir'),
		variableExpander: require('./variableExpander'),
		s3: require('./s3Helper')
	};
}
