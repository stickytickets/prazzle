var pluginLoader = require('./utils/packagePluginLoader');
var assert = require('better-assert');
var async = require('async');
var fs = require('fs');
var path = require('path');
var tmp = require('tmp');
var ncp = require('ncp').ncp;
var mkdirp = require('mkdirp');
var log = require('winston');
var gitVersion = require('./version');

var packageType, project, log, tempDir, version, outputDir;

exports.run = function(options, packageCallBack) {

    assert(options);

    outputDir = options.outputDir;
    packageType = options.packageType;
    project = options.project;
    version = options.version;

    assert('string' == typeof project);
    assert('string' == typeof packageType);

    log.info('Packaging starting for project: %s, packageType: %s', project, packageType);

    tmp.setGracefulCleanup();

    tempDir = tmp.dirSync({
        prefix: 'prazzle-',
        unsafeCleanup: true
    }).name;

    var done = function(error) {
        if (error) throw error;
        log.info('done');
    };


    var plugin = pluginLoader.load(options.packageType);

    pluginLoader.configure(options);

    options.outputDir = options.outputDir || ['..', 'dist', options.project].join(path.sep);

    cleanIfFlagged(options, function() {

        plugin.package(options, function(err, result) {
            if (err) throw err;

            if (result) {
                saveVersionToFile(options, function(err) { 
                    if(err) throw err;
                });
                async.forEachOf(result, function(value, key, cb) {
                        var metaDataFile = value.file + '.prazzle.json';
                        fs.writeFileSync(metaDataFile, JSON.stringify(value.metadata));
                    },
                    function(err) {
                        if (err) throw err;
                    });


            }

            log.info('Package Complete done');

            if (packageCallBack) packageCallBack(null, result);
        });
    })
}

function saveVersionToFile (options, cb){    
    var versionFile = options.versionFile || "version.txt";
    fs.writeFile(versionFile, options.version, function(err){
        if(err)
            cb(err, options);
        cb(null, options);
    });
}

function cleanIfFlagged(options, cb) {
    mkdirp(options.outputDir, function() {
        if (options.clean) {
            return options.fn.cleanDir(options.outputDir, function(err, r) {
                if (err) throw err;
                cb(null, options);
            });
        }
        return cb(null, options);
    });
}
