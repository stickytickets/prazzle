/*
Parameters:
•	secret-name: then name of the secret, e.g. XYZ_BILLING_PASSWORD
•	plainText: the sensitive text that needs to be encrypted
•	environment: the environment name for the secret
•	s3Bucket: read from env.PRAZZLE_S3_BUCKET unless supplied
•	kmsKey: the kms key to use for encryption, read from prazzle.config, unless supplied by command line
•   kmsRegion: the kms region to use for encryption, read from prazzle.config, unless supplied by command line
Description:
•	Encrypts the plain text using the kms key and kms service, stores the result in S3 [bucket-name] Key: secrets/[environment]/[secret-name]
*/

var assert = require('better-assert');
var AWS = require('aws-sdk');
var	log = require('winston');

exports.run = function(options) {

	assert(options);

	var secretName = options.secretName,
		plainText = options.plainText,
		environment = options.environment,
		s3Bucket = options.s3Bucket,
		kmsKey = options.kmsKey,
		kmsRegion = options.kmsRegion;


	assert('string' == typeof secretName);
	assert('string' == typeof plainText);
	assert('string' == typeof environment);
	assert('string' == typeof s3Bucket);
	assert('string' == typeof kmsKey);
	assert('object' == typeof log);

	log.info('Encrypting... (secret name: "%s", plain text: "%s", env: "%s", s3 bucket: "%s", kms key: "%s")',
		secretName, plainText, environment, s3Bucket, kmsKey);

	var s3 = new AWS.S3();
	var kms = new AWS.KMS({ region: kmsRegion });

	var encryptParams = {
		KeyId: kmsKey,
		Plaintext: plainText,
		  EncryptionContext: {
			  bucket: s3Bucket,
			  environment: environment,
			  secretName: secretName
		  }
	};

	kms.encrypt(encryptParams, function(err, data) {
		if (err) {
			console.log('Error encrypting the secret:')
			throw err;
		}

		var cipherText = data.CiphertextBlob.toString('base64');

		var params = {
			Bucket: s3Bucket,
			Key: 'environments/' + environment + "/secrets/" + secretName,
			ACL: "bucket-owner-full-control",
			Body: cipherText
		};

		s3.putObject(params, function(err, data) {
				if (err){
					console.log(err, err.stack);
				}
				else {
					console.log('Success: ',data);
				}
		});

	});
}
