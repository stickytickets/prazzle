var assert = require('better-assert');
var repositoryFactory = require("./utils/repository");
var prompt = require('prompt');
var extend = require('util')._extend;
var async = require('async');
var log = require('winston');

var repository, envKeys, envDefaults;

exports.run = function(options, callback) {

	assert(options);

	var environment = options.environment,
	s3Bucket = options.s3Bucket;

	assert('string' == typeof environment);
	assert('string' == typeof s3Bucket);

	repository = repositoryFactory(options);


	log.info('Creating Specification ... (env: "%s", s3 bucket: "%s")',	environment, s3Bucket);

	var spec = {
		projects: [],
		secrets: []
	};

	var x =spec;

	async.parallel([
		function(cb){ getProjectSpecs(options, function(err, r){ if(err) throw err; spec.projects = r; cb(); } ); },
		function(cb){ getSecrets(options, function(err, r){ if(err) throw err; spec.secrets = r;  cb(); } ); },
	], function(err, r){

		if(err) throw err;

		repository.setSpec({spec: spec, environment: options.environment}, function(err, r){
				if(err) throw err;
				log.info('OK');
				if(callback) return callback();
		})
	});
}

function getProjectSpecs(options, cb){

	var projectConfigs = [];

	repository.getPackageList(options, function(err, projects){

		if(err) throw err;

		log.info('Found Projects: ' + projects.join(', '));

		//download all config
		var projectConfigs = [];
		async.each(projects, function(projectName, callback) {
			repository.getConfig( extend(options, {project: projectName } ), function(err, result){
				if(err) return callback(err);

				projectConfigs.push(result);

				callback(null, result);
			})
		}, function(err){
			cb(err, projectConfigs);
		});
	});
}

function getSecrets(options, cb){

	var secrets = {};


	repository.getSecretList(options, function(err, results){

		results = results || [];
		log.debug('Got List Of Secrets, count: ' + results.join(', '));

		async.each(results, function(secret, callback) {
			repository.getSecret( extend(options, {secretName: secret } ), function(err, result){
				if(err) return callback(err);

				secrets[secret] = result;

				callback(null, result);
			})
		}, function(err){
			cb(err, secrets);
		});


	});
}
