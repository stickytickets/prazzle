/*
Parameters:
•	secret-name: then name of the secret, e.g. XYZ_BILLING_PASSWORD
•	environment: the environment name for the secret
•	s3Bucket: read from env.PRAZZLE_S3_BUCKET unless supplied
•   kmsRegion: read from prazzle.config, unless supplied by command line
Description:
•	Downloads the encrypted text from s3
•	Decrypts using kms service
•	Return plain text
*/

var assert = require('better-assert');
var assert = require('better-assert');
var AWS = require('aws-sdk');
var log = require('winston');

exports.run = function(options, cb) {

    assert(options);

    var secretName = options.secretName,
        environment = options.environment,
        s3Bucket = options.s3Bucket,
        kmsRegion = options.kmsRegion;


    assert('string' == typeof secretName);
    assert('string' == typeof environment);
    assert('string' == typeof s3Bucket);
    assert('string' == typeof kmsRegion);
    assert('object' == typeof log);

    log.info('Decrypting... (secret name: "%s", env: "%s", s3 bucket: "%s")', secretName, environment, s3Bucket);

    var s3 = new AWS.S3();
    var kms = new AWS.KMS({
        region: kmsRegion
    });

    var s3Params = {
        Bucket: s3Bucket,
        Key: 'environments/' + environment + "/secrets/" + secretName
    };

    s3.getObject(s3Params, function(err, data) {
        if (err) {
            console.log('Error downloading the secret: %s, from: %s\\%s - $s', secretName, s3Bucket, environment, err)
            return cb(null);
        }
        var base64String = data.Body.toString();

        var decryptParams = {
            CiphertextBlob: new Buffer(base64String, 'base64'),
            EncryptionContext: {
                bucket: s3Bucket,
                environment: environment,
                secretName: secretName
            }
        };

        kms.decrypt(decryptParams, function(err, data) {
            if (err) {
								console.log(decryptParams);
                console.log('Error decrypting the secret: ' + err);
                throw err;
            }

            if (cb) return cb(null,data.Plaintext.toString());

        });

    });
}
