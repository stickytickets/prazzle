var assert = require('better-assert');
var repositoryFactory = require("./utils/repository");
var prompt = require('prompt');
var extend = require('util')._extend;
var async = require('async');
var variableExpander = require('./utils/variableExpander');
var log = require('winston');
var fs = require('fs');

var repository, metadata, envDefaults, config, version, previousConfig = {};

exports.run = function(options, callback) {

    assert(options);

    var environment = options.environment,
        project = options.project,
        version = options.version,
        s3Bucket = options.s3Bucket,
        mode = options.mode;

    assert('string' == typeof environment);
    assert('string' == typeof project);
    assert('string' == typeof s3Bucket);
    assert('object' == typeof log);

    repository = repositoryFactory(options);


    if (options.versionFile) {
        readVersionFromFile(options);
        version = options.version;
    }

    if (options.remove) {
        return remove(options, function(error) {
            if (error) throw error;
            log.info('Done');
        });
    }

    options.autoMode = mode === 'auto';

    log.info('Setting version... (env: "%s", project: "%s", version: "%s", s3 bucket: "%s", mode: "%s")',
        environment, project, version, s3Bucket, mode);

    async.waterfall([
        function(cb) {
            cb(null, options)
        },

        getExistingConfig,
        getPackageMetaData,
        getEnv,
        updateSettings,
        validateSecrets,
        setConfig,
    ], function(error) {
        if (error) throw error;
        log.info('Done');
        if (callback) return callback();
    });

}

function readVersionFromFile(options, cb) {
    var versionFile = options.versionFile || "version.txt";
    if (fs.existsSync(versionFile)) {
        options.version = fs.readFileSync(versionFile, 'utf8');
    }

    if (cb) cb();
}

function remove(options, cb) {
    log.debug('Removing Config:', options)
    repository.removeConfig(options, cb);
}

function setConfig(options, cb) {
    config.environment = options.environment;
    options.config = config;
    repository.setConfig(options, cb);
}


function getExistingConfig(options, cb) {
    log.debug('Getting Current config:', options)
    repository.getConfig(options, function(err, result) {

        if (err) throw err;

        //No existing
        if (!result) {
            if (!options.version) throw "No existing package found, must supply a version."
            return cb(null, options);
        }

        previousConfig = result;

        if (!options.version) log.info('Configuring existing version:', result.version)

        options.version = options.version || result.version;

        cb(null, options);
    });
}


function getPackageMetaData(options, cb) {

	  log.debug('getPackageMetaData');
    repository.getPackageMetaData(options, function(err, packageJson) {
        if (err) throw err;
        metadata = packageJson;

				config = metadata;

        cb(null, options);
    });
}

function getEnv(options, cb) {
	log.debug('getEnv');
    repository.getEnv(options, function(err, defaults) {

        envDefaults = defaults;

        cb(null, options)
    });
}


function updateSettings(options, cb) {
	log.debug('updateSettings');
    var optionUpdateAndCb = function(err, updated) {
        config.env = updated;
        cb(null, options)
    };

    if (options.autoMode) {
        return updateSettingsFromDefaults(metadata.env, previousConfig.env || {}, envDefaults, optionUpdateAndCb);
    }

    return updateSettingsUsingPrompt(metadata.env, previousConfig.env || {}, envDefaults, optionUpdateAndCb);
}

function saveVersionToFile(options, cb) {
    var versionFile = options.versionFile || "version.txt";
    if (fs.existsSync(versionFile)) {
        options.version = fs.readFileSync(versionFile, 'utf8');
    }

    if (cb) cb();
}

function updateSettingsFromDefaults(keys, existingEnv, defaults, cb) {
    var properties = {};
    var errors = [];
    for (var k in keys) {
        properties[k] = existingEnv[k] || defaults[k];
        if (!properties[k]) errors.push(k);
    }

    cb(errors.length === 0 ? null : errors, properties);
}

function updateSettingsUsingPrompt(keys, existingEnv, defaults, cb) {
    //Loop through and cnofigure.
    var properties = {};

    for (var k in keys) {
        properties[k] = {
            required: true,
            default: existingEnv[k] || defaults[k] || keys[k],
        };
    }

    prompt.start();

    prompt.get({
        properties: properties
    }, function(err, updated) {
        cb(null, updated);
    });
}


function validateSecrets(options, cb) {

    repository.getSecretList(options, function(err, secrets) {

        if (err) return cb(err);

        var secretMap = {};

        for (var i = 0; i < secrets.length; i++) {
            secretMap[secrets[i]] = true;
        }


        var settings = extend({
            EMPTY: '',
            PRAZZLE_ENV: options.environment
        }, config.env);
        variableExpander.resolver(settings, function(k) {
            return secretMap.hasOwnProperty(k) ? 'OK' : null
        }).resolveSettings();
        cb(null, options);
    });

}
