var extend = require('util')._extend;
var helper = require('./helper');
var package = require.main.require('lib/package.js');

var validRequest = {
	packageType: 'test-package',
	project: 'test-project',
	version: '1.0.0',
	s3PublicBucket: 'test-bucket'
};

describe('package', function() {

	extend(package, helper);

	it('should throw error if project not supplied', function() {
		package.should_have_error_when_missing_argument(validRequest, 'project');
	});

	it('should throw error if version not supplied', function() {
		package.should_have_error_when_missing_argument(validRequest, 'version');
	});


});
