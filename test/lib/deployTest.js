var extend = require('util')._extend;
var helper = require('./helper');
var deploy = require.main.require('lib/deploy.js');

var validRequest = {
	name: 'test-name',
	environment: 'testing-env',
	s3Bucket: 'test-bucket',
	directory: 'src'
};

describe('deploy', function() {

	extend(deploy, helper);

	it('should throw error if name not supplied', function() {
		deploy.should_have_error_when_missing_argument(validRequest, 'name');
	});

	it('should throw error if environment not supplied', function() {
		deploy.should_have_error_when_missing_argument(validRequest, 'environment');
	});

	it('should throw error if s3Bucket not supplied', function() {
		deploy.should_have_error_when_missing_argument(validRequest, 's3Bucket');
	});

	it('should throw error if directory not supplied', function() {
		deploy.should_have_error_when_missing_argument(validRequest, 'directory');
	});

});
