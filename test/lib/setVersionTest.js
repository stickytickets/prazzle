var extend = require('util')._extend;
var helper = require('./helper');
var setConfig = require.main.require('lib/set-config.js');

var validRequest = {
	environment: 'testing-env',
	project: 'test-project',
	version: '1.0.0',
	s3Bucket: 'test-bucket'
};

describe('set-config', function() {

	extend(setConfig, helper);

	it('should throw error if environment not supplied', function() {
		setConfig.should_have_error_when_missing_argument(validRequest, 'environment');
	});

	it('should throw error if project not supplied', function() {
		setConfig.should_have_error_when_missing_argument(validRequest, 'project');
	});

	it('should throw error if version not supplied', function() {
		setConfig.should_have_error_when_missing_argument(validRequest, 'version');
	});
/*
	it('should throw error if key not supplied and value is', function() {
		setConfig.should_have_error_when_missing_argument(validRequest, 'key');
	});

	it('should throw error if value not supplied and key is', function() {
		setConfig.should_have_error_when_missing_argument(validRequest, 'value');
	});
	*/

	it('should throw error if s3Bucket not supplied', function() {
		setConfig.should_have_error_when_missing_argument(validRequest, 's3Bucket');
	});

});
