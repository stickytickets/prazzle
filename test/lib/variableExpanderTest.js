var assert = require('better-assert');
var variableExpander = require.main.require('lib/utils/variableExpander.js');


describe('variableExpander', function() {


	it('should resolve a settings', function(){

		var expander = variableExpander.resolver({ test: "test: ${test1}" , test1: "test1_value" })

		var result = expander.resolve('${test}');

		assert(result === 'test: test1_value');
	});

	it('should resolve a empty settings', function(){

		var expander = variableExpander.resolver({ test: "test: ${test1}" , test1: "${EMPTY}", EMPTY: '' })

		var result = expander.resolve('${test}');


		assert(result === 'test: ');
	});



});
