var assert = require('better-assert');
var extend = require('util')._extend;

exports.should_have_error = function(opts) {
	var error;

	try
	{
		this.run(opts);
	}
	catch(e)
	{
		error = e;
	}

	assert(error != null);
}

exports.should_have_error_when_missing_argument = function(validRequest, missingArg) {
	var opts = extend({}, validRequest);
	delete opts[missingArg];
	this.should_have_error(opts);
}
