var extend = require('util')._extend;
var envHelper = require.main.require('lib/utils/envHelper');
var assert = require('better-assert');

describe('envHelper', function(){

	it('should write and read keys', function(){
		envHelper.write('.env', {'key1':'12345'});
		keys = envHelper.read('.env');

		assert(keys['key1'] == '12345');
	})

	it('should write keys and should not overwrite the old content', function(){
	    
	    envHelper.write('.env',{'key2':'testing', 'keyA':'testkey1'});
		envHelper.write('.env',{'key2':'testing2'});
		keys = envHelper.read('.env');
		
		assert(keys['keyA'] == 'testkey1');
		assert(keys['key2'] == 'testing2');
	})

});