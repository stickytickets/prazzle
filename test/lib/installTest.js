var extend = require('util')._extend;
var helper = require('./helper');
var install = require.main.require('lib/install.js');

var validRequest = {
	packageType: 'test-package'
};

describe('install', function() {

	extend(install, helper);

	it('should throw error if packageType not supplied', function() {
		install.should_have_error_when_missing_argument(validRequest, 'packageType');
	});

});
