var assert = require('better-assert');
var util = require('util')
var cli = require.main.require('lib/cli.js');
var sinon = require('sinon');

//mock libs
var configure = require.main.require('lib/configure.js');
var package = require.main.require('lib/package.js');
var install = require.main.require('lib/install.js');
var deploy = require.main.require('lib/deploy.js');
var encrypt = require.main.require('lib/encrypt.js');
var decrypt = require.main.require('lib/decrypt.js');
var setConfig = require.main.require('lib/set-config.js');

describe('cli', function() {
	var calls = {};

	before(function() {
		sinon.stub(configure, 'run', function(options) { calls.configure = options; });
		sinon.stub(package, 'run', function(options) { calls.package = options; });
		sinon.stub(install, 'run', function(options) { calls.install = options; });
		sinon.stub(deploy, 'run', function(options) { calls.deploy = options; });
		sinon.stub(encrypt, 'run', function(options) { calls.encrypt = options; });
		sinon.stub(decrypt, 'run', function(options) { calls.decrypt = options; });
		sinon.stub(setConfig, 'run', function(options) { calls.setConfig = options; });
	});

	describe('configure', function() {

		it('should call configure with options', function() {
			cli.run('configure'.split(' '));

			assert(calls.configure !== null);
		});

	});

	describe('package', function() {

  		it('should call package with options', function() {
  			cli.run('package --packageType js --s3Bucket-public publicBucket --s3Bucket privateBucket'.split(' '));

  			assert(calls.package !== null);
  			assert(calls.package.packageType === 'js');
  		});

	});

	describe('install', function() {

  		it('should call install with options', function() {
  			cli.run('install --packageType js'.split(' '));

  			assert(calls.install !== null);
  			assert(calls.install.packageType === 'js');
  		});

	});

	describe('deploy', function() {

  		it('should call deploy with options', function() {
  			cli.run('deploy --source billing.zip --target deploy'.split(' '));

  			assert(calls.deploy !== null);
  			assert(calls.deploy.source === 'billing.zip');
  			assert(calls.deploy.target === 'deploy');
  		});

	});

	describe('encrypt', function() {

  		it('should call encrypt with options', function() {
  			cli.run('encrypt --secretName secret_key --plainText plain --environment testenv --s3Bucket bucket --kmsKey kms'.split(' '));

  			assert(calls.encrypt !== null);
  			assert(calls.encrypt.secretName === 'secret_key');
  			assert(calls.encrypt.plainText === 'plain');
  			assert(calls.encrypt.environment === 'testenv');
  			assert(calls.encrypt.s3Bucket === 'bucket');
  			assert(calls.encrypt.kmsKey === 'kms');
  		});

	});

	describe('decrypt', function() {

  		it('should call decrypt with options', function() {
  			cli.run('decrypt --secretName secret_key --environment testenv --s3Bucket bucket'.split(' '));

  			assert(calls.decrypt !== null);
  			assert(calls.decrypt.secretName === 'secret_key');
  			assert(calls.decrypt.environment === 'testenv');
  			assert(calls.decrypt.s3Bucket === 'bucket');
  		});

	});

	describe('set-config', function() {

  		it('should call set-config with options', function() {
  			cli.run('set-config --environment testenv --project billing --version 1.0.1 --key mykey --value myvalue --s3Bucket bucket'.split(' '));

  			assert(calls.setConfig !== null);
  			assert(calls.setConfig.environment === 'testenv');
  			assert(calls.setConfig.project === 'billing');
  			assert(calls.setConfig.version === '1.0.1');
  			assert(calls.setConfig.s3Bucket === 'bucket');
  		});

	});

	after(function() {
		//restore orginal
		configure.run.restore();
		package.run.restore();
		install.run.restore();
		deploy.run.restore();
		encrypt.run.restore();
		decrypt.run.restore();
		setConfig.run.restore();
	});
});
